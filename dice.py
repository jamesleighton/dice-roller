import random

print("Dice Rolling Simulator 2.0")
try:
	sides = int(input("How many sides? "))
	rolls = int(input("How many rolls? "))
except ValueError:
	print("That wasn't a number. Try Again.")
else:	
	x = 0
	while (x < rolls):
		print (random.randint(1,sides))
		x=x+1